help:
	@echo
	@echo "Please use \`make <target>' where <target> is one of"
	@echo
	@echo "  gen_reqs     to generate the pip requirements file in etc/"
	@echo "  autopep      to fix coding style in the project"
	@echo "  tests        to test project's tests (actually parsers)"
	@echo "  run_odl      to test odl docker for testing"
	@echo "  kill_odl     to kill odl docker"
	@echo "  rm_odl       to remove all  odl docker"
	@echo "  clear_pyc    to remove all pyc"


autopep:
	autopep8 --in-place --aggressive --aggressive -r .

clear_pyc:
	find . -name *.pyc* -type f -delete

gen_reqs:
	pipreqs .

run_odl:
	docker test -d -p 6633:6633 -p 8181:8181 -p 8101:8101 --name=opendaylight glefevre/opendaylight

kill_odl:
	docker ps | grep opendaylight | cut -d" " -f 1 | xargs docker kill

rm_odl:
	docker ps -a | grep opendaylight | cut -d" " -f 1 | xargs docker rm
