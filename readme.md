Readme:
======
ODL-segment-routing is used for setting up segment routing and EPE tunnels in the 
network core. This modules is part of a multi-module application.
Other modules are responsible for CDN prefix advertisements, 
openflow controller for white-boxes, visualisation of network topology using D3 module.
 
  
ODL-segment-routing discovered the IP network through BGP LS, sets up Segment routing tunnels,
and uses Egress Peer Engineering to label switch traffic to adjacent eBGP peers.

Additionally threads are available for monitoring the status of the Peer tunnel. so in case of flappings, 
the LSPs be deleted and avoid the risk of black holing traffic on the head end.



### Requirements:
tested under python 2.7.13 and then install the requirements.txt
* create virtuaalenv 
* pip install -r requirements

#### Opendaylight set up
Opendaylight version Carbon or higher

install BGP, PCEP, Netconf, Openflow enabled on it.
featue:install odl-restconf-all odl-netconf-all 
odl-netconf-connector-all odl-bgppcep-bgp-all odl-bgppcep-pcep-all

> for testing a docker file from docker hub has been used for setting up ODL



#### network set up:
* enable netconf on 830
* install BGP and enable BGP-LS and BGP EPE

````
router isis 1
 net <>
 distribute bgp-ls
 address-family ipv4 unicast
  metric-style wide
  mpls traffic-eng level-1-2
  mpls traffic-eng router-id Loopback0
  redistribute bgp 1
  segment-routing mpls
  segment-routing prefix-sid-map receive
  segment-routing prefix-sid-map advertise-local
 !
 interface Loopback0
  passive
  address-family ipv4 unicast
   prefix-sid index 1000
  !
 !
router bgp <>
 bgp router-id <>
 address-family ipv4 unicast
  network <>
 !
 ! iBGP
 ! iBGP peers
 !neighbor <ODL_IP>
 ! remote-as 1
 ! update-source Loopback0
 ! address-family ipv4 unicast
 ! !
 ! address-family link-state link-state
  !
 !
!
mpls traffic-eng
 interface <>
 !
 pce
  peer source ipv4 <head_node>
  peer ipv4 <address>
  !
  peer ipv4 <ODL_IP>
   precedence 5
  !
  segment-routing
  stateful-client
   instantiation
   delegation
  !
 !
 logging events all
 auto-tunnel pcc
  tunnel-id min 1 max 99
 !
!
netconf-yang agent
 ssh
!
segment-routing
!
ssh server v2
ssh server vrf Mgmt-intf
ssh server netconf vrf default
````

### How to test

The application has 2 modes of operation:
* single test mode to test atomic functions such setting up tunnels, discover the network, etc
* interactive mode which loops the user requests in ,and uses monitoring functions

using __main__ methods user can start the interactive mode.
The source can be used as library in other application. 
Demo.py is such example in which a Flask application which communicates with front end
uses ODL_segment_routing applications in single test mode.



