import unittest

from src.net_engine.api.odl import get_route_maps_native, post_staticRoute_tunnel, get_PCEP
from src.net_engine.logic.starter import Single_run, InteractiveCli
from src.net_engine.utils import read_const
import networkx as nx
from src.net_engine.core.graph.YanK import YenKShortestPaths
import requests

constants = read_const.Const()


class TestCases(unittest.TestCase):

    def test_odl(self):
        requests.get("http://127.0.0.1:8101/restconf/modules")

    def test_direct_pce(self):
        tid, mid = Single_run().set_PCE_tunnel(
            constants["node1"]["ip"], constants["node2"]["ip"], "test_tunnel")
        self.assertNotEqual(tid, '')

    def test_direct_staticroute(self):
        # direct_input().set_static_route("1.1.1.1", "4.2.2.4", "bijan4", 12)
        _, code = post_staticRoute_tunnel.run(
            constants['ODL']['ODL_IP'], constants["node1"]["name"], '43.43.43.46', '32', '5')
        self.assertEqual(code, 200)

    def test_get_pceptopo(self):
        _, code = get_PCEP.run("three")
        self.assertEqual(code, 200)

    def test_get_routemaps_native(self):
        _, code = get_route_maps_native.run(constants['ODL']['ODL_IP'],
                                            constants['ODL']['ODL_port'],
                                            constants['ODL']['ODL_user'],
                                            constants['ODL']['ODL_pass'],
                                            constants["node4"]["name"])
        self.assertEqual(code, 200)

    def test_interactive(self):
        InteractiveCli()

    def test_graph(self):
        G = nx.MultiDiGraph()
        G.add_edge('A', 'B', weight=4, capacity=200)
        G.add_edge('B', 'D', weight=200, capacity=200)
        G.add_edge('A', 'C', weight=30, capacity=200)
        G.add_edge('C', 'D', weight=4, capacity=200)

        ['A', 'B', 'D']
        YG = None
        YG = YenKShortestPaths(G)
        self.assertNotEqual(YG, None)

    #    API tests to be updaed
    def test_add_node(self):
        # script to add ip addresses to pop as cache and user, and to midtier
        pass

    def test_connectivity(self):
        # ce to pe, pe to p , pe to pe, pe to cache,
        pass

    def test_ODL_status(self):
        # get modules, uptime, system info of ODL
        pass

    def test_ODL_live(self):
        # get the topology of the network through netconf and linkstate
        pass

    def test_ODL_SR(self):
        # set up tunnels between the PEs,
        pass

    def test_ODL_steer(self):
        # use netconf to setup static routes on the routers
        pass

    def test_ODL_OF(self):
        # user openflow to clear flows first, and then add flows to the
        # whitebox
        pass

    def test_ODL_RS(self):
        # configure RS to advertise pop to RC / test this
        pass

    def test_ODL_PCE(self):
        # configure RS to advertise pop to RC / test this
        pass

def run():
    TC = TestCases()
    # TC.test_odl()
    TC.test_interactive()


if __name__ == "__main__.py":
    run()
