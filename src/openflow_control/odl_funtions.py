'''
second ODL controller used for openflow based white boxes
This is just for POC with constants
'''

import requests
import os


class ODLOpenflow(object):
    '''

    '''

    def __init__(self):
        '''

        '''
        self.odl_ip = 'four'
        self.odl_user = os.environ.get('ODL_USER', 'admin')
        self.odl_pass = os.environ.get('ODL_PASS', 'admin')
        self.switch1 = '5187633169'
        # self.switch1 = '87965226434561'
        self.req_hdrs = {'Content-Type': 'application/xml'}

    def url_builder(self, type, flow_id):
        '''

        :param flow_id:
        :return:
        '''
        url = ''
        if type == 1:
            url = 'http://' + self.odl_ip + ':8181' + \
                  '/restconf' \
                  '/config/opendaylight-inventory:nodes/node/openflow:' \
                  '' + self.switch1 + '/table/0/flow/' + str(flow_id)
            print "url is ", url
        elif type == 2:
            url = 'http://' + self.odl_ip + ':8181' + \
                  '/restconf' \
                  '/config/' \
                  'opendaylight-inventory:nodes/node/openflow:' \
                  '' + self.switch1 + '/table/0'
            print "url is ", url

        elif type == 3:
            url = 'http://' + self.odl_ip + ':8181' + \
                  '/restconf' \
                  '/operational/' \
                  'opendaylight-inventory:nodes/node/openflow:' \
                  '' + self.switch1 + '/table/0'
            print "url is ", url

        return url

    def message_builder(self):
        '''

        :return:
        '''
        message = '''<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <flow xmlns="urn:opendaylight:flow:inventory">
            <priority>2</priority>
            <flow-name>%s</flow-name>
            <match>
                <in-port>%s</in-port>
            </match>
            <id>%s</id>
            <table_id>0</table_id>
            <instructions>
                <instruction>
                    <order>0</order>
                    <apply-actions>
                        <action>
                           <order>0</order>
                           <output-action>
                               <output-node-connector>%s</output-node-connector>
                               <max-length>65535</max-length>
                           </output-action>
                        </action>
                    </apply-actions>
                </instruction>
            </instructions>
        </flow>
        '''
        return message

    def put_flow(self, flowname, flowid, inport, outport):
        url = self.url_builder(1, flowid)
        request_template = self.message_builder()
        req_body = request_template % (flowname, inport, flowid, outport)
        print req_body
        resp = requests.put(url, data=req_body, headers=self.req_hdrs,
                            auth=(self.odl_user, self.odl_pass))
        return resp.content

    def get_flow(self):
        '''

        :return:
        '''
        url = self.url_builder(3, '')
        resp = requests.get(url, headers=self.req_hdrs,
                            auth=(self.odl_user, self.odl_pass))
        return resp.content

    def del_flows(self):
        '''

        :return:
        '''
        url = self.url_builder(2, '')
        resp = requests.delete(url, headers=self.req_hdrs,
                               auth=(self.odl_user, self.odl_pass))
        return resp.content

    def test_del(self):
        # delete all
        print self.del_flows()

    def test_get(self):
        # delete all
        return self.get_flow()
    # @staticmethod

    def test_put(self):
        # delete all
        print self.put_flow("flow140", 14, 1, 4)
        print self.put_flow("flow410", 41, 4, 1)


def test():
    o = ODLOpenflow()
    # o.main()
    # o.del_flows()
    o.put_flow("flow140", 14, 1, 4)
    o.put_flow("flow410", 41, 4, 1)


if __name__ == "__main__.py":
    test()
