'''

This functions creates a topology model for D£ from LS.
This is for visualisation of the network

'''

from src.net_engine.services.topo_services import TopoBuilder
import json
import jinja2


class TopoJS:
    def __init__(self):
        self.tb = TopoBuilder()
        print "this is the TOPO JS service"
        for i in self.tb.nodes:
            print i.iso
        for i in self.tb.links:
            print i.source_net
        print self.tb.links

    def convert_topo(self):

        env = jinja2.Environment(
            loader=jinja2.PackageLoader(
                'plot', 'templates'))
        template = env.get_template("topo_hybrid.j2")
        # template = env.get_template("topo.j2")
        linkcounter = 0
        print(template.render(nodes=self.tb.nodes[0:len(self.tb.nodes) - 1],
                              lastnode=self.tb.nodes[len(self.tb.nodes) - 1],
                              links=self.tb.links[0:len(self.tb.links) - 1],
                              lastlink=self.tb.links[len(self.tb.links) - 1],
                              link_index=linkcounter))


if __name__ == "__main__.py":
    T = TopoJS()
    T.convert_topo()
