'''
Author          : Bijan R.Rofoee

'''
import time

from src.net_engine.core.netwatch import interface_stats_mon
from src.net_engine.utils import read_const


class MonitorIntfThreaded():
    def __init__(self, ODL_IP, ODL_PORT, NODE, INTF):
        self.t_mon = interface_stats_mon.EthMon(ODL_IP,
                                                ODL_PORT,
                                                NODE,
                                                INTF)
        self.scedulled_monitoring_iterative()

    def scedulled_monitoring_iterative(self):
        while (1):
            self.t_mon.run()
            time.sleep(4)  # delays for 5 seconds


def test():
    head_node = "xr1"
    intf_index = "0"
    C = read_const.Const()
    MonitorIntfThreaded(C.ODL_IP, C.ODL_port, head_node, intf_index)


if __name__ == "__main__.py":
    test()
