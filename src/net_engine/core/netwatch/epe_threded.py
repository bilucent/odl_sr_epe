'''
Author          : Bijan R.Rofoee

'''
from src.net_engine.services import rib_parser
from src.net_engine.utils import read_const
from src.net_engine.services.cleaner import CleanerClass

import logging as lg


class EpeThreaded():
    '''
    the objective is to react to the link fluctuations in the network
    '''

    def __init__(self, epe_nodes_lists, lsps, epe_orig):
        # Thread.__init__(self)
        c = read_const.Const()

        self.odlip = c.ODL_IP
        self.epenodes = epe_nodes_lists
        self.lsps = lsps
        self.epe_orig = dict(epe_orig)

    def run(self):
        sid_list = []
        temp_available_nodes_dic, labal_ip_dict = rib_parser.epefinder_rip(
            self.odlip)

        if len(temp_available_nodes_dic) != len(self.epe_orig):
            unavailble_nodes = self.unavaible_finder(temp_available_nodes_dic)
            #
            for ns in unavailble_nodes.values():

                lg.debug("epe link failed")
                sid_list.append(ns.peer_node_sid)

                cl = CleanerClass()
                cl.clean_chain(sid_list)
                # self.down_lsp(self.missing_epe(unavailble_nodes.values(),labal_ip_dict))
        else:
            pass

    def unavaible_finder(self, temp_available):
        temp_unavailable_dict = dict(self.epe_orig)
        for i in self.epe_orig.keys():

            if i not in temp_available.keys():
                del temp_unavailable_dict[i]

        return temp_unavailable_dict

    def missing_epe(self, temp_nodes, labal_ip_dict):
        missing_nodes = []
        for n in self.epenodes:
            for t in temp_nodes:
                lg.debug("EPE node label is missing")
                if n.remote_node_bgp_router_id != t.remote_node_bgp_router_id:
                    missing_nodes.append(n)
                    lg.debug(
                        "the missing node is: ",
                        t.link_v4_interface_address)

        return missing_nodes

    def update_lsp(self):
        pass

    def down_lsp(self, toBeMissedNodes):
        for l in self.lsps:
            pass
