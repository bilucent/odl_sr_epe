'''
Author          : Bijan R.Rofoee

'''
import json
import time

from src.net_engine.api.odl import get_interface_stats
import logging as lg


class EthMon():
    '''
    simply geting the counters of the packets in to the interface
    s an indicator of how the traffic flows to the node
    '''

    def __init__(self, IP, PORT, node, eth):
        self.ODL_IP = IP
        self.ODL_PORT = PORT
        self.ODL_node = node
        self.ODL_eth = eth

        pass

    def run(self):
        resp = get_interface_stats.run(self.ODL_IP, self.ODL_PORT,
                                       self.ODL_node, self.ODL_eth)
        results = json.loads(resp)
        for rib in results["statistic"]:
            first = rib["total-frames-transmitted"]
        time.sleep(1)  # delays for 5 seconds
        resp = get_interface_stats.run(self.ODL_IP, self.ODL_PORT,
                                       self.ODL_node, self.ODL_eth)
        results = json.loads(resp)

        for rib in results["statistic"]:
            second = rib["total-frames-transmitted"]

        if (int(second) - int(first)) > 400:
            lg.warning("Warning high util do something ")


def test():
    E = EthMon("127.0.0.1", "8181", "xr2", "1")
    E.run()


if __name__ == '__main__.py':
    test()
