'''
define link object store

'''


class LinkInfo():

    def __init__(self):

        # self.id = None
        self.routerid = None
        self.source = None
        self.destination = None
        self.source_net = None
        self.destination_net = None
        self.metric = None
        self.remoteid = None
        self.link_index = 0

    @property
    def x(self):
        """I'm the 'x' property."""
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    def __repr__(self):
        return str(self.routerid[10:] + " to " + self.remoteid[10:])
