'''
define EPE object store
'''


class BgpEpe():

    def __init__(self):
        self._x = None
        self.remote_node_as = None
        self.remote_node_bgp_router_id = None
        self.local_node_as = None
        self.local_node_domain = None
        self.local_node_bgp_router_id = None
        self.link_v4_interface_address = None
        self.link_v4_neighbour_address = None
        self.ipv4_next_hop = None
        self.peer_node_sid = None
        self.peer_node_weight = None

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value
