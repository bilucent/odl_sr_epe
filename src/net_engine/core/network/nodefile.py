'''
define node object store
'''


class NodeInfo():

    def __init__(self):

        self.name = None
        self.routerid = None
        self.net = None
        self.iso = None
        self.prefixes = list()

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value
