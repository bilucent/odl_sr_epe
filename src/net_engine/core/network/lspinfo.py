'''
define LSP object store
'''


class LspInfo():

    def __init__(self):
        self._x = None
        self.name = None
        self.node = None
        self.id = None
        self.src_ip = None
        self.dst_ip = None
        self.dic_iplabel = {}
        self.lst_ip = None
        self.API_command = None

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    def lsp_priner(self):
        self.name, " ",
        self.node, " ",
        self.id, " ",
        self.src_ip, " ",
        self.dst_ip, " ",
        self.dic_iplabel, " ",
        self.lst_ip
