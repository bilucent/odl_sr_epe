'''
run this in a separete thread. will check the LS and RIB to see any changes in topology information
'''
import sched
import time

from src.net_engine.core.netwatch import epe_threded


class MonitorEpeRibThreaded():
    def __init__(self, epe_nodes, lsps, epe_orig):
        self.t_mon = epe_threded.EpeThreaded(epe_nodes, lsps, epe_orig)
        self.scedulled_monitoring_iterative()

    def scedulled_monitoring_iterative(self):
        while (1):
            self.t_mon.run()
            time.sleep(3)  # delays for 5 seconds
