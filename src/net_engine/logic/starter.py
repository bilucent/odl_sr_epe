from __future__ import print_function
import requests

from src.net_engine.utils import read_const
from pyfiglet import figlet_format
from src.net_engine.core.netwatch import monitoring_interface_thread
from src.net_engine.services import tunneling_services
from src.net_engine.services import topo_services
from src.net_engine.services import pce_service
from src.net_engine.core.monitor import monitoring_services
import os
import sys
import threading
import json
import logging as lg
from colorama import init

from src.net_engine.api.odl import get_PCEP, get_modules
from src.net_engine.api.odl import get_route_maps_native
from src.net_engine.api.odl import post_staticRoute_tunnel
from src.net_engine.services.pcep_parser import pcep_parse

sys.path.append(os.path.dirname(__file__) + "/../..")


init(strip=not sys.stdout.isatty())  # strip colors if stdout is redirected


class ServiceMethods(object):
    def __init__(self):
        pass

    @staticmethod
    def collapsed_topo_service():
        try:
            s1 = topo_services.TopoBuilder()
            lg.debug("number of nodes: ", len(s1.nodes))
            lg.debug("number of links: ", len(s1.links))
            return s1
        except BaseException:
            pass


class InteractiveCli(object):
    class Colour:
        PURPLE = '\033[95m'
        CYAN = '\033[96m'
        DARKCYAN = '\033[36m'
        BLUE = '\033[94m'
        GREEN = '\033[92m'
        YELLOW = '\033[93m'
        RED = '\033[91m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'
        END = '\033[0m'

    class Messages:
        zero = 'Topo maker i.e discover my network and report me'
        one = 'Monitor my network (0 + 1)'
        two = 'PBR steering (create policies on the network)'
        three = 'StaticRoute steering'
        four = 'PCE + link failure'
        five = 'PCE + link failure + link_util'
        six = 'PCE internal'

    def __init__(self):
        print (
            InteractiveCli.Colour.BLUE +
            'Hello World !' +
            InteractiveCli.Colour.END)
        print (' please select your services from below by entering the associated numbers, separated by commas ')
        print (' [0] ' + InteractiveCli.Messages.zero)
        print (' [1] ' + InteractiveCli.Messages.one)
        print (' [2] ' + InteractiveCli.Messages.two)
        print (' [3] ' + InteractiveCli.Messages.three)
        print (' [4] ' + InteractiveCli.Messages.four)
        print (' [5] ' + InteractiveCli.Messages.five)
        print (' [6] ' + InteractiveCli.Messages.six)

        # print ' [6] ' + message.six

        option = raw_input("your function numbers: ")
        lg.debug(type(option))
        lg.debug('your option is %s' % option)

        # choices = []
        self.lsps = list()
        self.C = read_const.Const()
        choices = option.split(',')
        lg.debug('your listed numbers are as follow')

        lg.debug(sorted(choices))
        sorted_services_requested = sorted(choices)
        self.process_services(sorted_services_requested, self.lsps, self.C)

    def process_services(self, sorted_services_requested, lsps, const):

        # make sure ODL is up and BGP and PCEP are installed
        content, code = get_modules.run(
            const.ODL_IP, const.ODL_port, const.odl_user, const.odl_pass)
        assert code == 200
        json_data = json.loads(content.decode("utf-8"))
        module_list = json_data['modules']['module']
        module_names = [i['name'] for i in module_list]
        assert 'bgp' in module_names
        assert 'pcep' in module_names
        assert 'openflow' in module_names

        for req in sorted_services_requested:
            if str(req) == str(0):
                s1 = ServiceMethods.collapsed_topo_service()

            elif str(req) == str(1):
                m1 = monitoring_interface_thread.MonitorIntfThreaded(
                    const.ODL_IP, const.ODL_PORT, const.head_node, const.monitor_intf_index)
            elif str(req) == str(2):
                thread1 = threading.Thread(
                    target=monitoring_interface_thread.MonitorIntfThreaded, args=(
                        const.ODL_IP, const.ODL_PORT, const.head_node, const.monitor_intf_index))
                thread1.daemon = True  # Daemonize thread
                thread1.start()  # Start the execution
            elif str(req) == str(3):
                s1 = ServiceMethods.collapsed_topo_service()
                thread1 = threading.Thread(
                    target=monitoring_services.MonitorEpeRibThreaded, args=(
                        s1.epe, lsps, s1.epe_nodes_org))
                thread1.daemon = True  # Daemonize thread
                thread1.start()  # Start the execution
            elif str(req) == str(4):
                s1 = ServiceMethods.collapsed_topo_service()
                thread = threading.Thread(
                    target=monitoring_services.MonitorEpeRibThreaded, args=(
                        s1.epe, lsps, s1.epe_nodes_org))
                thread.daemon = True  # Daemonize thread
                thread.start()  # Start the execution
                # m1 = monitoring_services.Monitor_EPE_RIB_threaded(s1.epe,lsps)

                usrinput = str('yes')
                while (str(usrinput) == 'yes'):
                    srcPrefix = raw_input("src ip: ")
                    destPrefix = raw_input("dest ip: ")
                    tunnelName = raw_input("Tunnel_Interface_name: ")

                    p1 = pce_service.IpPathFinder(
                        s1.netgraph, srcPrefix, destPrefix)
                    s2 = tunneling_services.SegmentRouting(
                        const, s1.netgraph, p1.pa, s1.epe, srcPrefix, tunnelName, lsps)

                    # fid out the tunnel id based on the name given:
                    tid, mid, xsid = pcep_parse()

                    tun_id = tid[tunnelName]
                    mid_id = mid[tunnelName]
                    xsid_id = xsid[tunnelName]
                    s3 = post_staticRoute_tunnel.run(
                        const.ODL_IP, const.ODL_port, const.head_node, destPrefix, "32", tun_id)

                    while (str(usrinput) != 'yes' or str(usrinput) != 'no'):
                        usrinput = str(raw_input("again? yes/no: "))
                        # debug
                        if (str(usrinput) == 'yes'):
                            break
                        elif (str(usrinput) == 'no'):
                            exit()
            elif str(req) == str(5):

                threads = []

                s1 = ServiceMethods.collapsed_topo_service()

                thread = threading.Thread(
                    target=monitoring_services.MonitorEpeRibThreaded, args=(
                        s1.epe, lsps, s1.epe_nodes_org))

                # m1 = monitoring_services.Monitor_EPE_RIB_threaded(s1.epe,lsps)

                thread1 = threading.Thread(
                    target=monitoring_interface_thread.MonitorIntfThreaded, args=(
                        const.ODL_IP, const.ODL_PORT, const.tail_node, const.tail_intf_index))

                threads.append(thread)
                threads.append(thread1)

                thread.daemon = True  # Daemonize thread
                thread.start()  # Start the execution
                thread1.daemon = True  # Daemonize thread
                thread1.start()  # Start the execution

                usrinput = str('yes')
                while (str(usrinput) == 'yes'):
                    srcPrefix = raw_input("src ip: ")
                    destPrefix = raw_input("dest ip: ")
                    tunnelName = raw_input("Tunnel_Interface_name: ")

                    p1 = pce_service.IpPathFinder(
                        s1.netgraph, srcPrefix, destPrefix)
                    s2 = tunneling_services.SegmentRouting(
                        s1.netgraph, p1.pa, s1.epe, srcPrefix, tunnelName, lsps)

                    while (str(usrinput) != 'yes' or str(usrinput) != 'no'):
                        usrinput = str(raw_input("again? yes/no: "))
                        # debug
                        if (str(usrinput) == 'yes'):
                            break
                        elif (str(usrinput) == 'no'):
                            exit()
            elif str(req) == str(6):
                s1 = ServiceMethods.collapsed_topo_service()

                usrinput = str('yes')
                while (str(usrinput) == 'yes'):
                    srcPrefix = raw_input("src ip: ")
                    destPrefix = raw_input("dest ip: ")
                    tunnelName = raw_input("Tunnel_Interface_name: ")

                    p1 = pce_service.IpPathFinder(
                        s1.netgraph, srcPrefix, destPrefix)

                    # EPE finder PCE
                    # s2 = tunneling_services.SR(s1.netgraph, p1.pa, s1.epe, srcPrefix, tunnelName, lsps)

                    # normal PCE with internal nodes
                    s2 = tunneling_services.SegmentRoutingInternal(
                        const, s1.netgraph, p1.pa, srcPrefix, tunnelName, lsps, destPrefix)

                    # fid out the tunnel id based on the name given:
                    tid, mid, xsid = pcep_parse()

                    tun_id = tid[tunnelName]
                    mid_id = mid[tunnelName]
                    # xsid_id = xsid[tunnelName]
                    s3 = post_staticRoute_tunnel.run(
                        const.ODL_IP, const.ODL_PORT, const.head_node, destPrefix, "32", tun_id)

                    while (str(usrinput) != 'yes' or str(usrinput) != 'no'):
                        usrinput = str(raw_input("again? yes/no: "))
                        # debug
                        if (str(usrinput) == 'yes'):
                            break
                        elif (str(usrinput) == 'no'):
                            exit()

            else:
                lg.debug("option not available")
                exit(1)


class Single_run(object):
    def __init__(self):
        self.constants = read_const.Const()

    def set_PCE_tunnel(self, srcPrefix, destPrefix, tunnelName, nodename):
        lg.debug("running the pce tunnel set up")

        lsps = list()
        s1 = ServiceMethods.collapsed_topo_service()

        lg.debug("graph" + s1.netgraph)

        p1 = pce_service.IpPathFinder(s1.netgraph, srcPrefix, destPrefix)
        lg.debug(" before lsps length: ", len(lsps))

        # normal PCE with internal nodes
        s2 = tunneling_services.SegmentRoutingInternal(
            self.constants, s1.netgraph, p1.pa, srcPrefix, tunnelName, lsps, destPrefix)

        lg.debug(" after lsps length: ", len(lsps))

        # fid out the tunnel id based on the name given:
        tid, mid, xsid = pcep_parse()

        tun_id = ''
        mid_id = ''

        tun_id = tid[tunnelName]
        mid_id = mid[tunnelName]

        lg.debug("tun_id:" + tun_id)
        lg.debug("mid_id:" + mid_id)
        return tun_id, mid_id

    def set_static_route(
            self,
            srcPrefix,
            destPrefix,
            tunnelName,
            tun_id,
            nodename):
        # nodename just added, but i have a function  C.PCC_NODES.get(src)
        # which finds the PCC node from the source address
        C = read_const.Const()
        lg.debug("running the static route set up")
        post_staticRoute_tunnel.run(
            C.ODL_IP,
            C.ODL_port,
            nodename,
            destPrefix,
            "32",
            tun_id)


if __name__ == "__main__.py":
    InteractiveCli()
#
# class Test:
#     C = read_const.Const()
#
#     @staticmethod
#     def test_direct_pce():
#         Single_run().set_PCE_tunnel("1.1.1.1", "2.2.2.2", "bijan4")
#
#     @staticmethod
#     def test_direct_staticroute():
#         # direct_input().set_static_route("1.1.1.1", "4.2.2.4", "bijan4", 12)
#         post_staticRoute_tunnel.run('three', 'xr1', '43.43.43.46', '32', '5')
#
#     @staticmethod
#     def test_get_pceptopo():
#         return get_PCEP("three")
#
#     @staticmethod
#     def test_get_routemaps_native():
#         return get_route_maps_native.run("three", "xed")
#
#     @staticmethod
#     def test_interactive():
#         Interactive_cli()
#
#
# if __name__ == "__main__.py":
#     Test()
#     Test.test_interactive()
#     Test.test_direct_pce()
#     Test.test_direct_staticroute()
