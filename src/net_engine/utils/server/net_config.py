'''
fix the network on the hosting server
'''
import paramiko
import getpass


class RemoteServer:
    """

    """

    def __init__(self):
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # self.p = getpass.getpass()
        self.ssh.connect('five', username='netdev1', password='netdev1')

    def set_lo(self, loip, loid):
        lo_template = "sudo ifconfig lo:%s %s netmask 255.255.255.255 up "
        ssh_body = lo_template % (loid, loip)
        stdin, stdout, stderr = self.ssh.exec_command(ssh_body)
        stdin.write("netdev1\n")

        self.ssh.close()


if __name__ == "__main__.py":
    re = RemoteServer()
    re.set_lo("12.12.12.12", "12")
