'''
a number of constants. extract from a config file
'''
import yaml
import os


class Const(object):
    def __init__(self):
        self.config_dict = self.read()

        self.head_node = self.config_dict['head_node']
        self.head_intf_index = self.config_dict['head_intf_index']

        self.tail_node = self.config_dict['tail_node']
        self.tail_intf_index = self.config_dict['tail_intf_index']

        self.ODL_IP = self.config_dict['ODL']['ODL_IP']
        self.ODL_port = self.config_dict['ODL']['ODL_port']

        self.NODE_LABELS = {
            self.config_dict["node1"]["ip"]: self.config_dict["node1"]["sid"],
            self.config_dict["node2"]["ip"]: self.config_dict["node2"]["sid"],
            self.config_dict["node3"]["ip"]: self.config_dict["node3"]["sid"],
            self.config_dict["node6"]["ip"]: self.config_dict["node6"]["sid"]}

        self.PCC_NODES = {
            self.config_dict["node1"]["ip"]: "pcc://" + str(
                self.config_dict["node1"]["sid"]),
            self.config_dict["node2"]["ip"]: "pcc://" + str(
                self.config_dict["node2"]["sid"])}

        self.ROUTER_NAMES = {
            self.config_dict["node1"]["ip"]: self.config_dict["node1"]["name"],
            self.config_dict["node2"]["ip"]: self.config_dict["node2"]["name"],
            self.config_dict["node3"]["ip"]: self.config_dict["node3"]["name"],
            self.config_dict["node6"]["ip"]: self.config_dict["node6"]["name"]}

        self.odl_user = ""
        self.odl_pass = ""
        self.odl_user = os.environ.get('ODL_USER', 'admin')
        self.odl_pass = os.environ.get('ODL_PASS', 'admin')
        if self.odl_user == "":
            self.odl_user["ODL"]["ODL_user"]
            self.odl_pass["ODL"]["ODL_pass"]

    def read(self):
        # os.chdir("../../../etc")
        print(os.getcwd())
        config_file = "etc/constant.yaml"
        with open(config_file) as conf:
            try:
                return yaml.load(conf)
            except yaml.YAMLError as e:
                exit(1)


def ftest():
    Readconfig = Const()


if __name__ == "__main__.py":
    ftest()
