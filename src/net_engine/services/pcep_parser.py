
# this piece of code looks deep in the lsp informaiton from pcep
# and associates each tunnel id with the tunnel name. this is needed when creating static routes
# whcih should point to te tunnel id rather the lsp name

#  this returns a dictionary mapping of tunnel names and their ids and sid as the mpls label of the last hop.
#  this will be used when cleaning the lsps after a failure

import json

from src.net_engine.api.odl import get_PCEP
from src.net_engine.utils.read_const import Const
import logging as lg


def pcep_parse():
    dict_name2tid = {}
    dict_name2mid = {}
    dict_name2xsid = {}

    C = Const()
    lst = get_PCEP.run(C.ODL_IP)
    lstopo = json.loads(lst)
    nodes = []
    for top in lstopo["topology"]:
        for n in top["node"]:
            try:
                for p in n["network-topology-pcep:path-computation-client"]["reported-lsp"]:
                    name = p["name"]
                    #
                    for pa in p["path"]:
                        tid = pa["odl-pcep-ietf-stateful07:lsp"]["tlvs"]["lsp-identifiers"]["tunnel-id"]
                        mid = pa["odl-pcep-ietf-stateful07:lsp"]["tlvs"]["path-binding"]["mpls-label"]
                        dict_name2mid[name] = mid
                        dict_name2tid[name] = tid
                        for er in pa["ero"]["subobject"]:
                            sid = er["odl-pcep-segment-routing:sid"]
                            dict_name2xsid[name] = sid

            except BaseException:
                pass

    return dict_name2tid, dict_name2mid, dict_name2xsid
