'''
Author          : Bijan R.Rofoee

'''
from src.net_engine.core.graph.ModDijk import ModifiedDijkstra


class IpPathFinder(object):
    def __init__(self, graph, srcPrefix, destPrefix):
        self.g = graph
        self.src = srcPrefix
        self.dest = destPrefix
        self.pa = self.path()

    def path(self):
        MG = ModifiedDijkstra(self.g)
        path = MG.getPath(self.src, self.dest, False)
        return path
