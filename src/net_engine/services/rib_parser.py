'''
Author          : Bijan R.Rofoee

'''
# this piece of code looks deep in the lsp informaiton from pcep
# and associates each tunnel id with the tunnel name. this is needed when creating static routes
# whcih should point to te tunnel id rather the lsp name

#  this returns a dictionary mapping of tunnel names and their ids and sid as the mpls label of the last hop.
#  this will be used when cleaning the lsps after a failure

import json

from src.net_engine.api.odl import get_RIB
from src.net_engine.core.network import node_epepeer


def epefinder_rip(odl_ip):
    resp = get_RIB.run(odl_ip)
    label_ip_dict = {}
    results = json.loads(resp)
    output = []
    epenodes_dic = {}
    # for each peer get "tables" "bgp-linkstate:linkstate-routes"
    # "protocol-id"=="bgp-epe"
    for rib in results["rib"]:
        for beer in rib["peer"]:
            for table in beer["effective-rib-in"]["tables"]:
                if "bgp-linkstate:linkstate-routes" not in table.keys():
                    continue
                # "bgp-linkstate:linkstate-routes" is there
                lstable = table["bgp-linkstate:linkstate-routes"]["linkstate-route"]
                # lstable = table["bgp-linkstate:linkstate-routes"]["route-key"]
                for route in lstable:
                    if route["protocol-id"] == "bgp-epe":
                        p = node_epepeer.BgpEpe()
                        output.append(route)
                        p.peer_node_sid = route["attributes"]["link-attributes"]["peer-node-sid"]["local-label"]
                        p.remote_node_bgp_router_id = route["remote-node-descriptors"]["bgp-router-id"]
                        p.local_node_bgp_router_id = route["local-node-descriptors"]["bgp-router-id"]
                        p.ipv4_next_hop = route["attributes"]["ipv4-next-hop"]["global"]
                        p.link_v4_interface_address = route["link-descriptors"]["ipv4-interface-address"]
                        p.link_v4_neighbour_address = route["link-descriptors"]["ipv4-neighbor-address"]
                        p.local_node_as = route["local-node-descriptors"]["as-number"]
                        label_ip_dict[p.link_v4_interface_address] = p.peer_node_sid
                        epenodes_dic[p.link_v4_interface_address] = p
                    else:
                        continue

    return epenodes_dic, label_ip_dict

