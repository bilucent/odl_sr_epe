'''
Author          : Bijan R.Rofoee

'''

import json
import re
import logging as lg
import networkx as nx

from src.net_engine.api.odl import get_LS, get_RIB
from src.net_engine.core.graph.ModDijk import ModifiedDijkstra
from src.net_engine.core.network import linkfile, node_epepeer
from src.net_engine.core.network import nodefile
from src.net_engine.services import rib_parser
from src.net_engine.utils import read_const


class TopoBuilder(object):

    def __init__(self):
        C = read_const.Const()

        self.IP = C.ODL_IP
        self.nodes = self.node_finder()
        self.links = self.link_finder()

        # this is for EPE, not used for demo
        self.epe, self.epe_xsid_dict = rib_parser.epefinder_rip(self.IP)
        self.epe_nodes_org = dict(self.epe)

        self.iso_id = {}
        self.node_searcher(self.iso_id, self.nodes)
        self.netgraph = self.graphmaker(self.nodes, self.links)

    def route_find(self, head, tail):
        return nx.shortest_path(self.netgraph, head, tail, weight='weight')

    def node_searcher(self, dref, nodes):
        for n in nodes:
            dref[n.iso] = n.routerid

    def node_finder(self):
        lst = get_LS.run(self.IP)
        lstopo = json.loads(lst)
        nodes = []
        for top0 in lstopo["topology"]:
            for nodetopo in top0["node"]:
                if "IsisLevel2" in nodetopo["node-id"]:
                    n = nodefile.NodeInfo()
                    n.name = nodetopo["l3-unicast-igp-topology:igp-node-attributes"]["name"]
                    for id in nodetopo["l3-unicast-igp-topology:igp-node-attributes"]["router-id"]:
                        n.routerid = str(id)
                    for pref in nodetopo["l3-unicast-igp-topology:igp-node-attributes"]["prefix"]:
                        for i in pref:
                            n.prefixes.append(pref["prefix"])
                    for netinf in nodetopo["l3-unicast-igp-topology:igp-node-attributes"]["isis-topology:isis-node-attributes"]["net"]:
                        n.net = str(netinf)
                    n.iso = str(nodetopo["l3-unicast-igp-topology:igp-node-attributes"][
                                "isis-topology:isis-node-attributes"]["iso"]["iso-system-id"])

                    nodes.append(n)

        return nodes

    def link_finder(self):
        lst = get_LS.run(self.IP)
        lstopo = json.loads(lst)
        links = []
        for top0 in lstopo["topology"]:
            for linktopo in top0["link"]:
                # not sure why i had 0004 excluded?????
                # if "IsisLevel2" in linktopo["link-id"] and \
                #                 "local-router=0000.0000.0004" not in linktopo["link-id"]:
                if "IsisLevel2" in linktopo["link-id"]:
                    # local - router = 0000.0000.0004
                    l = linkfile.LinkInfo()
                    l.name = linktopo["link-id"]
                    name_split = l.name.split('-router=')
                    l.routerid = str(name_split[1][:14])
                    l.remoteid = str(name_split[2][:14])

                    ln = re.findall(r'[0-9]+(?:\.[0-9]+){2}', l.name)
                    l.source_net = str(ln[0])
                    l.destination_net = str(ln[1])
                    l.metric = linktopo["l3-unicast-igp-topology:igp-link-attributes"]["metric"]
                    u_src = linktopo["source"]["source-tp"]
                    ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', u_src)
                    try:
                        l.source = str(ip[0])
                    except BaseException:
                        l.source = 'NaN'
                    u_dest = linktopo["destination"]["dest-tp"]
                    ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', u_dest)
                    try:
                        l.destination = str(ip[0])
                    except BaseException:
                        l.destination = 'NaN'
                    l.link_index = int(
                        l.routerid[12:]) * 100 + int(l.remoteid[12:])
                    links.append(l)
        lg.debug(
            + links
        )
        return links

    def epefinder_rip(self):
        resp = get_RIB.run(self.IP)
        labal_ip_dict = {}
        results = json.loads(resp)
        output = []
        epenodes = []
        # for each peer get "tables" "bgp-linkstate:linkstate-routes"
        # "protocol-id"=="bgp-epe"
        for rib in results["rib"]:
            for beer in rib["peer"]:
                for table in beer["effective-rib-in"]["tables"]:
                    if "bgp-linkstate:linkstate-routes" not in table.keys():
                        continue
                    # "bgp-linkstate:linkstate-routes" is there
                    lstable = table["bgp-linkstate:linkstate-routes"]["linkstate-route"]
                    # lstable = table["bgp-linkstate:linkstate-routes"]["route-key"]
                    for route in lstable:
                        if route["protocol-id"] == "bgp-epe":
                            p = node_epepeer.BgpEpe()
                            output.append(route)
                            p.peer_node_sid = route["attributes"]["link-attributes"]["peer-node-sid"]["local-label"]
                            p.remote_node_bgp_router_id = route["remote-node-descriptors"]["bgp-router-id"]
                            p.local_node_bgp_router_id = route["local-node-descriptors"]["bgp-router-id"]
                            p.ipv4_next_hop = route["attributes"]["ipv4-next-hop"]["global"]
                            p.link_v4_interface_address = route["link-descriptors"]["ipv4-interface-address"]
                            p.link_v4_neighbour_address = route["link-descriptors"]["ipv4-neighbor-address"]
                            p.local_node_as = route["local-node-descriptors"]["as-number"]

                            labal_ip_dict[p.link_v4_interface_address] = p.peer_node_sid
                            epenodes.append(p)

                        else:
                            continue

        return epenodes, labal_ip_dict

    def graphmaker(self, nodes, links):
        GMnode = nx.DiGraph()

        for l in links:

            GMnode.add_edge(self.iso_id[l.source_net],
                            self.iso_id[l.destination_net],
                            weight=l.metric,
                            capactiy=10,
                            label="IS",
                            adj_src=l.source,
                            adj_dest=l.destination)

        # i need to get lists of value of the epe dictionary to add the nodes
        # to the topo

        # this is for EPE, not used for demo
        # v = self.epe.values()
        # for e in v:
        #     GMnode.add_edge(str(e.local_node_bgp_router_id), str(e.remote_node_bgp_router_id), weight= 0,
        #                     capactiy=10, label = "EPE",
        # adj_src = str(e.link_v4_interface_address), adj_dest =
        # str(e.link_v4_neighbour_address))

        return GMnode
