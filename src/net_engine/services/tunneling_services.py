
from src.net_engine.api.odl.post_PCEP import segmentrouting
from src.net_engine.api.odl.post_PCEP import segmentrouting_epe
from src.net_engine.core.network import lspinfo


class SegmentRouting(object):
    def __init__(
            self,
            Constants,
            graph,
            path,
            epe_nodes,
            src,
            tunnelName,
            lsps):

        self.IP = Constants.ODL_IP
        self.port = Constants.ODL_port
        self.lsps = lsps

        ls_dest = path[0][0]
        link_dict = graph.edge[path[0][0]][path[0][1]]

        ls_dst_edge_label = self.find_epe(
            epe_nodes.values(), link_dict['adj_src'])
        self.setup_sr_epe(
            Constants.PCC_NODES.get(src),
            tunnelName,
            ls_dest,
            src,
            Constants.NODE_LABELS.get(ls_dest),
            link_dict['adj_src'],
            ls_dst_edge_label)

    def setup_sr_epe(
            self,
            pcc,
            tunname,
            ls_dst,
            src,
            ls_dst_label,
            ls_dst_adj_ip,
            ls_dst_edge_label):
        l = lspinfo.LspInfo()
        l = segmentrouting_epe(
            self.IP,
            self.port,
            pcc,
            tunname,
            ls_dst,
            src,
            ls_dst_label,
            ls_dst_adj_ip,
            ls_dst_edge_label)

        self.lsps.append(l)

    def find_epe(self, nodes, ip):
        for n in nodes:
            if n.link_v4_interface_address in ip:
                return n.peer_node_sid
        return None


class SegmentRoutingInternal(object):
    def __init__(self, Constants, graph, path, src, tunnelName, lsps, dest):

        self.IP = Constants.ODL_IP
        self.lsps = lsps

        ls_dest = dest
        link_dict = graph.edge[path[0][0]][path[0][1]]

        self.setup_sr(
            Constants.PCC_NODES.get(src),
            tunnelName,
            ls_dest,
            src,
            Constants.NODE_LABELS.get(ls_dest),
            link_dict['adj_src'])

    def setup_sr(self, pcc, tunname, ls_dst, src, ls_dst_label, ls_dst_adj_ip):
        l = lspinfo.LspInfo()
        l = segmentrouting(
            self.IP,
            pcc,
            tunname,
            ls_dst,
            src,
            ls_dst_label,
            ls_dst_adj_ip)

        self.lsps.append(l)

    def find_epe(self, nodes, ip):
        for n in nodes:
            if n.link_v4_interface_address in ip:
                return n.peer_node_sid
        return None
