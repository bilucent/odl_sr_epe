
from src.net_engine.api.odl import post_del_PCEP
from src.net_engine.services import pcep_parser
from src.tests import constants


class CleanerClass(object):
    '''
    in dev state: clean up the lsp information: this class needs to be completed
    '''

    def __init__(self):
        self.C = constants.Const()

    def clean_chain(self, sids):
        t, m, s = pcep_parser.pcep_parse()

        for a in sids:
            name = self.name_from_sid(s, a)
            post_del_PCEP.del_lsp(self.C.ODL_IP, "pcc://1.1.1.1", name)

    def name_from_sid(self, sid_dict, a):
        key = next(key for key, value in sid_dict.items() if value == a)
        return key


if __name__ == "__main__.py":
    c = CleanerClass()
    c.clean_static_routes()
    # clean_static_routes()
