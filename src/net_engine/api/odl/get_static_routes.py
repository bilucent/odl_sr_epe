
'''
get all the static routes stored in the default vrf table (GRT)
'''

import logging as lg
import requests


def run(ODL_IP, rest_port, odl_user, odl_pass, node):

    req_hdrs = {'Content-Type': 'application/json'}

    url = 'http://' + ODL_IP + ':' + rest_port + '/restconf/config/network-topology:network-topology/topology/' \
        'topology-netconf/node/' + node + '/yang-ext:mount/' \
        'Cisco-IOS-XR-ip-static-cfg:router-static/default-vrf/address-family/vrfipv4/vrf-unicast/vrf-prefixes/'

    try:

        resp = requests.get(url, headers=req_hdrs, auth=(odl_user, odl_pass))

        return resp.content, resp.status_code

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)
