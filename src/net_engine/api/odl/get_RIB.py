'''
get BGP rib
'''

import logging as lg
import requests


def run(ODL_IP, rest_port, odl_user, odl_pass):

    rib_id = "example-bgp-rib"

    req_hdrs = {'Content-Type': 'application/json'}

    url = 'http://' + ODL_IP + ':' + rest_port + \
        '/restconf/operational/bgp-rib:bgp-rib/rib/' + rib_id

    try:

        resp = requests.get(url, headers=req_hdrs, auth=(odl_user, odl_pass))

        return resp.content, resp.status_code

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)
