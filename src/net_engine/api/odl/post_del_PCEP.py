'''
delete a PCEP tunnel.
'''

import logging as lg

import requests


def run(ODL_IP, rest_port, odl_user, odl_pass, node, name):

    request_template = '''
    <input xmlns="urn:opendaylight:params:xml:ns:yang:topology:pcep">
     <node>%s</node>
     <name>%s</name>
     <network-topology-ref xmlns:topo="urn:TBD:params:xml:ns:yang:network-topology">/topo:network-topology/topo:topology[topo:topology-id="pcep-topology"]</network-topology-ref>
    </input>
    '''
    req_hdrs = {'Content-Type': 'application/xml'}

    req_body = request_template % (node, name)

    url = 'http://' + ODL_IP + rest_port + \
          '/restconf/operations/network-topology-pcep:remove-lsp'

    try:
        resp = requests.post(
            url,
            data=req_body,
            headers=req_hdrs,
            auth=(
                odl_user,
                odl_pass))

        return resp.content, resp.status_code

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)

