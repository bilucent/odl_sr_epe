import requests
import logging as lg

class StaticRoutes(object):
    def __init__(self):
        pass

    def staticroute_nextintf(self):
        pass

    def staticroute_nexthop(
            self,
            ODL_IP,
            rest_port,
            odl_user,
            odl_pass,
            node,
            prefix,
            prefix_length,
            next_hop):
        request_template = '''
        {
          "vrf-prefix": [
            {
              "prefix": "%s",
              "prefix-length": %s,
              "vrf-route": {
                "vrf-next-hop-table": {
                  "vrf-next-hop-next-hop-address": [
                    {
                      "next-hop-address": "%s"
                    }
                  ]
                }
              }
            }
          ]
        }
        '''
        req_hdrs = {'Content-Type': 'application/json'}

        req_body = request_template % (prefix, prefix_length, next_hop)

        url = 'http://' + ODL_IP + ':' + rest_port + \
              '/restconf/config/network-topology:network-topology/topology' + \
              '/topology-netconf/node/' + node + '/yang-ext:mount' + \
              '/Cisco-IOS-XR-ip-static-cfg:router-static/default-vrf' + \
              '/address-family/vrfipv4/vrf-unicast/vrf-prefixes/vrf-prefix/' + \
              prefix + '/' + prefix_length

        try:

            resp = requests.put(
                url,
                data=req_body,
                headers=req_hdrs,
                auth=(
                    odl_user,
                    odl_pass))

            return resp.content, resp.status_code

        except Exception as e:
            excepName = type(e).__name__
            lg.error("request error: " + excepName)
            exit(1)
