'''
Create a PCEP tunnel
'''

import logging as lg
import requests

from src.net_engine.core.network import lspinfo


def segmentrouting_epe(
        ODL_IP,
        rest_port,
        odl_user,
        odl_pass,
        node,
        name,
        dest_ip,
        src_ip,
        tail_sid,
        tail_adj_ip,
        head_sid):
    '''
    post PCE tunnel for EPE enabled tunnel
    '''
    request_template = '''
    {
      "input": {
        "node": "%s",
        "name": "%s",
        "network-topology-ref":"/network-topology:network-topology/
        network-topology:topology[network-topology:topology-id=\\"pcep-topology\\"]",
        "arguments": {
          "lsp": {
            "delegate": true,
            "administrative": true
          },
          "endpoints-obj": {
            "ipv4": {
              "destination-ipv4-address": "%s",
              "source-ipv4-address": "%s"
            }
          },
          "path-setup-type": {
            "pst": 1
          },
          "ero": {
            "subobject": [
              {
                "loose": false,
                "sid-type": "ipv4-node-id",
                "sid": "%s",
                "m-flag": true,
                "ip-address": "%s"
              },
              {
                "loose": true,
                "sid-type": "ipv4-adjacency",
                "sid": "%s",
                "m-flag": true
              }
            ]
          }
        }
      }
    }
    '''
    req_hdrs = {'Content-Type': 'application/json'}

    req_body = request_template % (
        node, name, dest_ip, src_ip, tail_sid, dest_ip, head_sid)

    url = 'http://' + ODL_IP + rest_port + \
          '/restconf/operations/network-topology-pcep:add-lsp'

    try:
        resp = requests.post(
            url,
            data=req_body,
            headers=req_hdrs,
            auth=(
                odl_user,
                odl_pass))

        #

        lsp_ = lspinfo.LspInfo()
        lsp_.node = node
        lsp_.name = name
        lsp_.id = None
        lsp_.src_ip = src_ip
        lsp_.dst_ip = dest_ip
        lsp_.dic_iplabel = {dest_ip: tail_sid, tail_adj_ip: head_sid}
        lsp_.lst_ip = tail_adj_ip

        return resp.content, resp.status_code, lsp_

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)


def segmentrouting(
        ODL_IP,
        rest_port,
        odl_user,
        odl_pass,
        node,
        name,
        dest_ip,
        src_ip,
        tail_sid,
        tail_adj_ip):
    '''

    post PCE tunnel for normal tunnel

    '''
    request_template = '''
    {
      "input": {
        "node": "%s",
        "name": "%s",
        "network-topology-ref":"/network-topology:network-topology/
        network-topology:topology[network-topology:topology-id=\\"pcep-topology\\"]",
        "arguments": {
          "lsp": {
            "delegate": true,
            "administrative": true
          },
          "endpoints-obj": {
            "ipv4": {
              "destination-ipv4-address": "%s",
              "source-ipv4-address": "%s"
            }
          },
          "path-setup-type": {
            "pst": 0
          },
          "ero": {
            "subobject": [
              {
                "loose": true,
                "sid-type": "ipv4-node-id",
                "sid": "%s",
                "m-flag": false,
                "ip-address": "%s"
              }
            ]
          }
        }
      }
    }
    '''
    req_hdrs = {'Content-Type': 'application/json'}

    req_body = request_template % (
        node, name, dest_ip, src_ip, tail_sid, dest_ip)

    url = 'http://' + ODL_IP + rest_port + \
          '/restconf/operations/network-topology-pcep:add-lsp'

    try:

        resp = requests.post(
            url,
            data=req_body,
            headers=req_hdrs,
            auth=(
                odl_user,
                odl_pass))

        #

        lsp_ = lspinfo.LspInfo()
        lsp_.node = node
        lsp_.name = name
        lsp_.id = None
        lsp_.src_ip = src_ip
        lsp_.dst_ip = dest_ip
        lsp_.dic_iplabel = {dest_ip: tail_sid}
        lsp_.lst_ip = tail_adj_ip
        # l.API_command = str(req_hdrs + "/n" + req_body)

        return resp.content, resp.status_code, lsp_

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)
