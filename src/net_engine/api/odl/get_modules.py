'''
Author          : Bijan R.Rofoee
'''
import requests
import logging as lg

def run(ODL_IP, rest_port, odl_user, odl_pass):

    req_hdrs = {'Content-Type': 'application/json'}

    url = "http://" + ODL_IP + ":" + str(rest_port) + "/restconf/modules"

    try:
        resp = requests.get(url, headers=req_hdrs, auth=(odl_user, odl_pass))

        return resp.content, resp.status_code

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)
