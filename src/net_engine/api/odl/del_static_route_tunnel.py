import requests
import logging as lg
'''
Author          : Bijan R.Rofoee

'''

"""
put-static-route.

adds static route to XR

parameter:
* ODL IP address
* Peer XR NETCONF node
* prefix
* length
* next-hop

uses HTTP PUT with JSON payload
"""


def run(
        ODL_IP,
        rest_port,
        odl_user,
        odl_pass,
        node,
        prefix,
        prefix_length,
        tunnel_id):
    request_template = '''
    {
      "vrf-prefix": [
        {
          "prefix": "%s",
          "prefix-length": %s,
          "vrf-route": {
            "vrf-next-hop-table": {
                "vrf-next-hop-interface-name": [
                  {
                    "interface-name": "tunnel-te%s"
                  }
              ]
            }
          }
        }
      ]
    }
    '''

    req_hdrs = {'Content-Type': 'application/json'}

    req_body = request_template % (prefix, prefix_length, tunnel_id)

    url = 'http://' + ODL_IP + ':' + rest_port + \
          '/restconf/config/network-topology:network-topology/topology' + \
          '/topology-netconf/node/' + node + '/yang-ext:mount' + \
          '/Cisco-IOS-XR-ip-static-cfg:router-static/default-vrf' + \
          '/address-family/vrfipv4/vrf-unicast/vrf-prefixes/vrf-prefix/' + \
          prefix + '/' + prefix_length

    try:
        resp = requests.delete(
            url,
            data=req_body,
            headers=req_hdrs,
            auth=(
                odl_user,
                odl_pass))

        return resp.content, resp.status_code

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)
