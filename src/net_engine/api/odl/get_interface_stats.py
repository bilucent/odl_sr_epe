'''
read the interface counters
'''
import os
import requests
import logging as lg

def run(ODL_IP, rest_port, odl_user, odl_pass, node, eth_port):

    req_hdrs = {'Content-Type': 'application/json'}

    url = 'http://' + ODL_IP + ':' + rest_port + '' \
        '/restconf/operational/network-topology:network-topology/' \
        'topology/topology-netconf/node/' + node + '/yang-ext:mount/' \
        'Cisco-IOS-XR-drivers-media-eth-oper:ethernet-interface/statistics/statistic/' \
        'GigabitEthernet0%2F0%2F0%2F' + str(eth_port)

    try:
        resp = requests.get(url, headers=req_hdrs, auth=(odl_user, odl_pass))

        return resp.content, resp.status_code

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)
