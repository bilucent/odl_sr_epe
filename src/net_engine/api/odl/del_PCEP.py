'''
Author          : Bijan R.Rofoee
User rest call to ODL to remove a LSP
'''
import os
import requests
import logging as lg


def run(ODL_IP, rest_port, odl_user, odl_pass, LSP_name):
    request_template = '''
    <input xmlns="urn:opendaylight:params:xml:ns:yang:topology:pcep">
     <node>pcc://1.1.1.1</node>
     <name>%s</name>
     <network-topology-ref xmlns:topo="urn:TBD:params:xml:ns:yang:network-topology">/topo:network-topology/topo
     :topology[topo:topology-id="{{pcep-topology-id}}"]</network-topology-ref>
    </input>"
    '''

    req_hdrs = {'Content-Type': 'application/xml'}

    url = 'http://' + ODL_IP + ':' + rest_port + \
        '/restconf/operations/network-topology-pcep:remove-lsp'
    try:
        resp = requests.delete(url, headers=req_hdrs, auth=(odl_user, odl_pass))

        return resp.content, resp.status_code

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)
