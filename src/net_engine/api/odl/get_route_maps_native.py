
'''
For IOS XE with native yang. get route maps
'''

import logging as lg
import requests


def run(ODL_IP, rest_port, odl_user, odl_pass, node):

    req_hdrs = {'Content-Type': 'application/json'}

    url = 'http://' + ODL_IP + ':' + rest_port + \
          '/restconf/operational/network-topology:network-topology/topology' \
          '/topology-netconf/node/' + node + '/yang-ext:mount/Cisco-IOS-XE-native:native'

    try:
        resp = requests.get(url, headers=req_hdrs, auth=(odl_user, odl_pass))

        return resp.content, resp.status_code

    except Exception as e:
        excepName = type(e).__name__
        lg.error("request error: " + excepName)
        exit(1)
