import os
import sys
import json
from datetime import timedelta
from functools import update_wrapper
from pprint import pprint as pp


# from Tkinter import *
from src.openflow_control.odl_funtions import ODLOpenflow
from src.net_engine.logic import starter
from src.cdn_map import input as inp
from src.net_engine.api.odl import get_PCEP
from src.net_engine.api.odl import get_route_maps_native
from src.net_engine.api.odl import get_static_routes
from src.net_engine.api.odl import post_staticRoute_tunnel
from src.net_engine.api.odl.put_staticRoute import StaticRoutes

from flask import Flask, jsonify, request, current_app, make_response
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)

cors = CORS(app, resources={r'/*': {"origins": '*'}})

app.config['CORS_HEADERS'] = 'Content-Type'


@app.before_request
def log_request_info():
    app.logger.debug('Headers: %s', request.headers)
    app.logger.debug('Body: %s', request.get_data())


def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add(
        'Access-Control-Allow-Headers',
        'Content-Type,Authorization')
    response.headers.add(
        'Access-Control-Allow-Methods',
        'GET,PUT,POST,DELETE,OPTIONS')
    return response


@app.route('/control/rs_api/v1.0/rmw', methods=['POST'])
def rmw():
    print "this is RMW"
    data = json.loads(request.get_data())
    rs_c = inp.direct_input()
    rs_c.odl_write_rm(data['CDNinfo'])
    return jsonify(i_am='writing!')


@app.route('/control/rs_api/v1.0/rma', methods=['POST'])
def rma():
    print "this is RMA"
    data = json.loads(request.get_data())
    rs_c = inp.direct_input()
    rs_c.odl_del_rm_all(data['CDNinfo'])
    return jsonify(i_am='removing all!')


@app.route('/control/rs_api/v1.0/rmr', methods=['POST'])
def rmr():
    print "this is RMR"
    data = json.loads(request.get_data())
    rs_c = inp.direct_input()
    rs_c.odl_del_rm(data['CDNinfo'])
    return jsonify(i_am='removing!')


@app.route('/control/rs_api/v1.0/rmc', methods=['POST'])
def rmc():
    print "this is RMC"
    data = json.loads(request.get_data())
    rs_c = inp.direct_input()
    rs_c.odl_check_rm_pl(data['CDNinfo'])
    return jsonify(i_am='checking!')


@app.route('/control/rs_api/v1.0/getrm', methods=['GET'])
def get_rm_rs():
    print "this is rmget"
    return get_route_maps_native.run("three", "xed")


# below methods are used for routing and tunnel creation on the P routers
@app.route('/control/rt_api/v1.0/staticnh', methods=['POST'])
def static_route_nh():
    print "this is static"
    data = json.loads(request.get_data())
    st = StaticRoutes()
    st.staticroute_nexthop('three', data['nodename'], data['dstip'],
                           data['dstpx'], data['nexthop'])

    return jsonify(i_am='putting!')

# below methods are used for routing and tunnel creation on the P routers


@app.route('/control/rt_api/v1.0/static', methods=['POST'])
def static_route():
    print "this is static"
    data = json.loads(request.get_data())
    post_staticRoute_tunnel.run('three', data['nodename'], data['dstip'],
                                data['dstpx'], data['tunid'])
    return jsonify(i_am='writing!')


@app.route('/control/rt_api/v1.0/getstatic', methods=['GET'])
def get_static_route():
    print "this is get static routes"
    return get_static_routes.run('three', 'xr1')


@app.route('/control/rt_api/v1.0/srt', methods=['POST'])
def sr_tunnel():
    print "this is srt"
    data = json.loads(request.get_data())
    pce_c = starter.Single_run()
    pce_c.set_PCE_tunnel(data['srcip'], data['dstip'],
                         data['tunname'], data['nodename'])
    return jsonify(i_am='writing!')


@app.route('/control/rt_api/v1.0/getpcesrt', methods=['GET'])
def get_PCE_sr_tunnel():
    print "this is get srt"
    return get_PCEP.run("three")


@app.route('/control/rt_api/v1.0/oft_create', methods=['POST'])
def of_tunnel_create():
    print "this is OF tunnel create"
    o = ODLOpenflow()
    o.test_put()
    return jsonify(i_am='writing OF!')


@app.route('/control/rt_api/v1.0/oft_del', methods=['POST'])
def of_tunnel_delete():
    print "this is OF tunnel delete"
    o = ODLOpenflow()
    o.test_del()
    return jsonify(i_am='deleting OF!')


@app.route('/control/rt_api/v1.0/oft_get', methods=['GET'])
def get_OF_tunnels():
    print "this is OF tunnel get"
    o = ODLOpenflow()
    data = o.test_get()
    return data


class Demo:
    def __init__(self):
        # d = json.load("etc/start.json")
        file = open("etc/start.json", "r")
        start_data = json.load(file)
        pp(start_data["ODL_core"])


class Test:
    def __init__(self):
        pass

    def test_add_node(self):
        # script to add ip addresses to pop as cache and user, and to midtier
        pass

    def test_connectivity(self):
        # ce to pe, pe to p , pe to pe, pe to cache,
        pass

    def test_ODL_status(self):
        # get modules, uptime, system info of ODL
        pass

    def test_ODL_live(self):
        # get the topology of the network through netconf and linkstate
        pass

    def test_ODL_SR(self):
        # set up tunnels between the PEs,
        pass

    def test_ODL_steer(self):
        # use netconf to setup static routes on the routers
        pass

    def test_ODL_OF(self):
        # user openflow to clear flows first, and then add flows to the
        # whitebox
        pass

    def test_ODL_RS(self):
        # configure RS to advertise pop to RC / test this
        pass

    def test_ODL_PCE(self):
        # configure RS to advertise pop to RC / test this
        pass


if __name__ == '__main__.py':

    print get_OF_tunnels()
    print get_PCE_sr_tunnel()
    print get_rm_rs()
    print get_static_route()
    # static_route()

    app.run(host='0.0.0.0', port=5002, debug=True)
